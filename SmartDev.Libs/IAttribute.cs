﻿namespace SmartDev.Libs
{
    public interface IAttribute<T>
    {
        T Value { get; }
    }
}