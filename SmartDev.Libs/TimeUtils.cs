﻿using System;

namespace SmartDev.Libs
{
    public class TimeUtils
    {
        public static string GetDiffString(DateTime dateTime)
        {
            return TimeUtils.GetDiffString(DateTime.Now - dateTime);
        }

        public static string GetDiffString(TimeSpan timeDiff)
        {
            int totalSeconds = (int)timeDiff.TotalSeconds;
            if (totalSeconds < 0)
                return "not yet";
            if (totalSeconds < 60)
            {
                if (timeDiff.Seconds != 1)
                    return timeDiff.Seconds.ToString() + " seconds ago";
                return "one second ago";
            }
            if (totalSeconds < 120)
                return "a minute ago";
            if (totalSeconds < 2700)
                return timeDiff.Minutes.ToString() + " minutes ago";
            if (totalSeconds < 5400)
                return "an hour ago";
            if (totalSeconds < 86400)
                return timeDiff.Hours.ToString() + " hours ago";
            if (totalSeconds < 172800)
                return "yesterday";
            if (totalSeconds < 2592000)
                return timeDiff.Days.ToString() + " days ago";
            if (totalSeconds < 31104000)
            {
                int int32 = Convert.ToInt32(Math.Floor((double)timeDiff.Days / 30.0));
                if (int32 > 1)
                    return int32.ToString() + " months ago";
                return "one month ago";
            }
            int int32_1 = Convert.ToInt32(Math.Floor((double)timeDiff.Days / 365.0));
            if (int32_1 > 1)
                return int32_1.ToString() + " years ago";
            return "one year ago";
        }

        public static DateTime FirstDayOfWeek(DateTime weekDate, DayOfWeek firstDay = DayOfWeek.Monday)
        {
            int num = firstDay - weekDate.DayOfWeek;
            return weekDate.AddDays((double)num);
        }
    }
}
