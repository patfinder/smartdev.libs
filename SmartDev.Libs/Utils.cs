﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;

namespace SmartDev.Libs
{
    public class Utils
    {
        // TODO: reorganize this class, mixed functionalities
        private static Random random = new Random((int)DateTime.Now.Ticks);

        public static string RandomString(int size, bool lowerCase = false)
        {
            StringBuilder stringBuilder = new StringBuilder();
            Random random = new Random();
            for (int index = 0; index < size; ++index)
            {
                char ch = Convert.ToChar(Convert.ToInt32(random.Next(65, 87)));
                stringBuilder.Append(ch);
            }
            if (lowerCase)
                return stringBuilder.ToString().ToLower();
            return stringBuilder.ToString();
        }

        public static void SendEmail(string server, int port, string emailFrom, string user, string password, string[] emailTos, string subject, string body)
        {
            Utils.SendEmail(server, port, emailFrom, (string)null, user, password, emailTos, subject, body);
        }

        public static void SendEmailAsync(string server, int port, string emailFrom, string user, string password, string[] emailTos, string subject, string body)
        {
            new Thread((ThreadStart)(() => Utils.SendEmail(server, port, emailFrom, (string)null, user, password, emailTos, subject, body))).Start();
        }

        public static void SendEmail(string server, int port, string emailFrom, string emailTitle, string user, string password, string[] emailTos, string subject, string body)
        {
            emailTitle = emailTitle ?? emailFrom;
            MailMessage mail = new MailMessage();
            mail.Subject = subject;
            mail.Sender = new MailAddress(emailFrom, emailTitle);
            mail.From = mail.Sender;
            mail.IsBodyHtml = true;
            mail.Body = body;
            ((IEnumerable<string>)emailTos).ToList<string>().ForEach((Action<string>)(a => mail.To.Add(a)));
            SmtpClient smtpClient = new SmtpClient(server, port)
            {
                EnableSsl = true
            };
            smtpClient.Credentials = (ICredentialsByHost)new NetworkCredential(user, password);
            smtpClient.Send(mail);
        }

        public static T GetObjectFromJson<T>(string json)
        {
            // TODO: move to data type utils
            return (T)JsonConvert.DeserializeObject(json, typeof(T));
        }

        public static object GetObjectFromJson(string json, Type type)
        {
            return JsonConvert.DeserializeObject(json, type);
        }

        public static string GetJsonFromObject(object obj)
        {
            throw new NotImplementedException("Fix below code");
            //JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
            //  serializerSettings.set_NullValueHandling((NullValueHandling) 1);
            //  return JsonConvert.SerializeObject(obj, serializerSettings);
        }
    }
}
