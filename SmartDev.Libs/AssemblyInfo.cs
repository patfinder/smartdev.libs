﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("SmartDev.Libs")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2013")]
[assembly: AssemblyTitle("SmartDev.Libs")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SmartDev")]
[assembly: ComVisible(false)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: Guid("3155c392-5a20-4826-8c10-372f172242bd")]
[assembly: AssemblyVersion("1.0.0.0")]
