﻿namespace DebtManagerWin
{
    public static class AssignmentUtils
    {
        public static void AssignValue(object obj, string propName, object value)
        {
            var props = obj.GetType().GetProperties();
            foreach (var prop in props)
            {
                if(prop.Name == propName)
                {
                    prop.SetValue(obj, value);
                    break;
                }
            }
        }
    }
}
