﻿using Newtonsoft.Json;
using System;
using System.Security.Cryptography;
using System.Text;

namespace SmartDev.Libs
{
    public class AuthenticationStruct
    {
        public DateTime TimeStamp = DateTime.MinValue;
        private const int MIN_KEY_LENGTH = 40;
        public int UserID;
        public string Name;
        public string Authenticator;

        private static string GetAuthenticator(AuthenticationStruct authStruct)
        {
            MD5CryptoServiceProvider cryptoServiceProvider = new MD5CryptoServiceProvider();
            string name = authStruct.Name;
            string empty = string.Empty;
            for (int index = 0; index < 40; ++index)
                empty += "a";
            string s = name.Length < 40 ? name + empty : name;
            return Convert.ToBase64String(cryptoServiceProvider.ComputeHash(new UTF8Encoding().GetBytes(s)));
        }

        public static AuthenticationStruct FromUser(int ID, string name)
        {
            AuthenticationStruct authStruct = new AuthenticationStruct()
            {
                TimeStamp = DateTime.Now,
                UserID = ID,
                Name = name
            };
            authStruct.Authenticator = GetAuthenticator(authStruct);
            return authStruct;
        }

        public string GetAccessToken(byte[] key, byte[] IV)
        {
            return Crypto.EncryptAESB64(this.ToJsonString(), key, IV);
        }

        public bool Authenticate()
        {
            return this.Authenticator == GetAuthenticator(this);
        }

        public string ToJsonString()
        {
            // TODO: ToJsonString
            throw new NotImplementedException("Fix below code");
            //JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
            //serializerSettings.set_NullValueHandling(NullValueHandling.Ignore);
            //return JsonConvert.SerializeObject((object) this, serializerSettings);
        }

        public static AuthenticationStruct FromJsonString(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<AuthenticationStruct>(json);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
