﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartDev.Libs
{
    public class AuthorizeLib
    {
        private static Random rand = new Random((int)DateTime.Now.Ticks);

        public static string GetRandomToken()
        {
            string str = Guid.NewGuid().ToString();
            List<string> list1 = ((IEnumerable<string>)str.Substring(1, str.Length - 2).Split('-')).ToList<string>();
            List<string> list2 = list1.Take<string>(2).ToList<string>();
            List<string> list3 = list1.Skip<string>(2).ToList<string>();
            list3.AddRange((IEnumerable<string>)new string[1]
            {
        string.Format("{0:X4}", (object) AuthorizeLib.rand.Next(1000)).ToLower()
            });
            list3.AddRange((IEnumerable<string>)list2);
            return string.Join("-", (IEnumerable<string>)list3);
        }
    }
}
