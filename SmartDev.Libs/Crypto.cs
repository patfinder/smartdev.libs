﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace SmartDev.Libs
{
    public class Crypto
    {
        private static string _Entropy = "NexleLib Entropy";

        public static string Entropy
        {
            get
            {
                if (string.IsNullOrEmpty(Crypto._Entropy))
                    throw new InvalidOperationException("_Entropy not set");
                return Crypto._Entropy;
            }
        }

        public static string EncryptB64(string plainText)
        {
            return Crypto.EncryptB64(Crypto.Entropy, plainText);
        }

        public static string EncryptB64(string entropy, string plainText)
        {
            if (string.IsNullOrEmpty(entropy))
                throw new ArgumentNullException(nameof(entropy));
            if (plainText == null)
                throw new ArgumentNullException(nameof(plainText));
            byte[] bytes = Encoding.UTF8.GetBytes(entropy);
            return Convert.ToBase64String(ProtectedData.Protect(Encoding.Unicode.GetBytes(plainText), bytes, DataProtectionScope.LocalMachine));
        }

        public static string DecryptB64(string cipher)
        {
            return Crypto.DecryptB64(Crypto.Entropy, cipher);
        }

        public static string DecryptB64(string entropy, string cipher)
        {
            if (string.IsNullOrEmpty(entropy))
                throw new ArgumentNullException(nameof(entropy));
            if (string.IsNullOrEmpty(cipher))
                throw new ArgumentNullException(nameof(cipher));
            return Encoding.Unicode.GetString(ProtectedData.Unprotect(Convert.FromBase64String(cipher), Encoding.UTF8.GetBytes(entropy), DataProtectionScope.LocalMachine));
        }

        public static string EncryptAESB64(string plainText, byte[] Key, byte[] IV)
        {
            return Convert.ToBase64String(Crypto.EncryptAES(plainText, Key, IV));
        }

        public static byte[] EncryptAES(string plainText, byte[] Key, byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException(nameof(plainText));
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException(nameof(Key));
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException(nameof(Key));
            using (AesManaged aesManaged = new AesManaged())
            {
                aesManaged.Key = Key;
                aesManaged.IV = IV;
                ICryptoTransform encryptor = aesManaged.CreateEncryptor(aesManaged.Key, aesManaged.IV);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                            streamWriter.Write(plainText);
                        return memoryStream.ToArray();
                    }
                }
            }
        }

        public static string DecryptAESB64(string base64Text, byte[] Key, byte[] IV)
        {
            return Crypto.DecryptAES(Convert.FromBase64String(base64Text), Key, IV);
        }

        public static string DecryptAES(byte[] cipherText, byte[] Key, byte[] IV)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException(nameof(cipherText));
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException(nameof(Key));
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException(nameof(Key));
            using (AesManaged aesManaged = new AesManaged())
            {
                aesManaged.Key = Key;
                aesManaged.IV = IV;
                ICryptoTransform decryptor = aesManaged.CreateDecryptor(aesManaged.Key, aesManaged.IV);
                using (MemoryStream memoryStream = new MemoryStream(cipherText))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                            return streamReader.ReadToEnd();
                    }
                }
            }
        }
    }
}
