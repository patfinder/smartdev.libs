﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SmartDev.Libs
{
    public static class ReflectionUtils
    {
        public static MemberInfo GetMemberInfo<T>(string memberName)
        {
            return typeof(T).GetProperties().FirstOrDefault(p => p.Name == memberName);
        }

        public static MemberInfo GetMemberInfo(this Type type, string memberName)
        {
            return type.GetProperties().FirstOrDefault(p => p.Name == memberName);
        }

        public static object GetPropertyValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src);
        }

        public static object GetPropertyValue(object src, PropertyInfo propInfo)
        {
            return propInfo.GetValue(src);
        }
    }
}
