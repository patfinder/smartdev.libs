﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SmartDev.Libs
{
    public static class AttributeUtils
    {
        /// <summary>
        /// Get attribute value of all property for specified attribute.
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="A">Attribute Type</typeparam>
        /// <returns></returns>
        public static Dictionary<string, A> GetAttributeValues<T, A>()
        {
            var attrValues = new Dictionary<string, A>();
            var objType = typeof(T);

            var props = objType.GetProperties();
            foreach (var prop in props)
            {
                var attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    if (attr is A)
                    {
                        attrValues.Add(prop.Name, (A)attr);
                    }
                }
            }

            return attrValues;
        }

        /// <summary>
        /// Get attribute of object member and other ICustomAttributeProvider
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <param name="provider">Something like a MemberInfo</param>
        /// <returns></returns>
        public static T GetAttributeValue<A, T>(this ICustomAttributeProvider provider) where A : IAttribute<T>
        {
            var attributes = provider.GetCustomAttributes(typeof(A), true);
            return attributes.Length > 0 ? ((A)attributes[0]).Value : default(T);
        }

        /// <summary>
        /// Get attribute of object member and other ICustomAttributeProvider
        /// </summary>
        /// <typeparam name="A"></typeparam>
        /// <param name="provider">Something like a MemberInfo</param>
        /// <returns></returns>
        public static A GetAttribute<A>(this ICustomAttributeProvider provider) where A : Attribute
        {
            var attributes = provider.GetCustomAttributes(typeof(A), true);
            return attributes.Length > 0 ? attributes[0] as A : null;
        }

        /// <summary>
        /// Get attribute for class
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="A"></typeparam>
        /// <returns></returns>
        public static A GetAttribute<T, A>() where A : Attribute
        {
            return GetAttribute<A>(typeof(T));
        }

        /// <summary>
        /// Get attribute for member
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="A"></typeparam>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public static A GetAttribute<T, A>(string memberName) where A : Attribute
        {
            return GetAttribute<A>(ReflectionUtils.GetMemberInfo<T>(memberName));
        }

        public static A GetAttribute<A>(MemberInfo memberInfo) where A : Attribute
        {
            return GetAttribute<A>((ICustomAttributeProvider)memberInfo);
        }
        
        /// <summary>
        /// Get DisplayName Attribute value of a object member
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exp"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetDisplayName<T>(Expression<T> exp, string defaultValue)
        {
            return GetDisplayName(exp) ?? defaultValue;
        }

        /// <summary>
        /// Get DisplayName Attribute value of a object member
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exp"></param>
        /// <returns></returns>
        public static string GetDisplayName<T>(Expression<T> exp)
        {
            var attr = GetAttribute<DisplayNameAttribute>((exp.Body as MemberExpression).Member);
            return attr?.DisplayName;
        }

        public static string GetDisplayName(MemberInfo memberInfo, string defaultValue)
        {
            return GetDisplayName(memberInfo) ?? defaultValue;
        }

        public static string GetDisplayName(MemberInfo memberInfo)
        {
            return GetAttribute<DisplayNameAttribute>(memberInfo)?.DisplayName;
        }

        public static string GetDisplayName<T>()
        {
            var objType = typeof(T);
            var attr = objType.GetCustomAttribute<DisplayNameAttribute>(true);
            return attr?.DisplayName;
        }

        public static string GetTypeDisplayName<T>(string defaultValue)
        {
            return GetDisplayName<T>() ?? defaultValue;
        }
    }
}
