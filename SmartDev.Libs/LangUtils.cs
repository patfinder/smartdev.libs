﻿using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace SmartDev.Libs
{
    public class LangUtils
    {
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod()
        {
            return new StackTrace().GetFrame(1).GetMethod().Name;
        }
    }
}
