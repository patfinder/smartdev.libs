﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDev.Libs
{
    public class FileUtils
    {
        public static string ReplaceInvalidFileNameChars(string fileName, string alternative = "-")
        {
            string invalidChars = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

            foreach (char ch in invalidChars)
            {
                fileName = fileName.Replace(ch.ToString(), alternative);
            }

            return fileName;
        }
    }
}
